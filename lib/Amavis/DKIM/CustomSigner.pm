package Amavis::DKIM::CustomSigner;
use strict;
use re 'taint';
use warnings;
use warnings FATAL => qw(utf8 void);
no warnings 'uninitialized';
# use warnings 'extra'; no warnings 'experimental::re_strict'; use re 'strict';

sub new {
  my($class,%params) = @_;
  bless { %params }, $class;
}

sub sign_digest {
  my($self_key, $digest_alg_name, $digest) = @_;
  my $code = $self_key->{CustomSigner};
  &$code($digest_alg_name, $digest, %$self_key);
}

1;
